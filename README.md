# Firestorm Dice Bot

A simple bot designed to roll dice and nothing more. Contains a power expression
engine that allows for arbitrarily compliated 4-operation expressions.

## Usage

Invoke the command using the prefix `$roll`, and then type the expression you
wish to compute. The parser accepts only integer numbers and `<count>d<sides>`
dice notation, but utilizes Clojure's numeric tower and can do any operation
with perfect precision.

Sample:

```
<<< $roll 2d6 + 4

>>> • 2d6 → 2 + 6 = 8
>>> Final total = 12
```

## License

Copyright © 2021 Robert Cochran

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
