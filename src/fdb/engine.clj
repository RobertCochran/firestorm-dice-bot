(ns fdb.engine
  (:require [instaparse.core :as insta]
            [clojure.edn :as edn]
            [clojure.string :as string]))

(def expr-grammar (slurp (clojure.java.io/resource "grammar.txt")))

(def expr-parser (insta/parser expr-grammar))

(def operator? #{:mult :div :add :sub})

(defn reorder-operator
  "Reorder EXPR from infix notation to parenthesized reverse-Polish notation"
  [expr]
  (if (and (sequential? expr)
           (operator? (second expr)))
    (let [[init-lh init-op init-rh & rh-rest] expr]
      (loop [expr-rest rh-rest
             acc [init-op init-lh init-rh]]
        (if (empty? expr-rest)
          acc
          (recur (drop 2 expr-rest)
                 (let [[rh-op rh-val] expr-rest]
                   (if (= (first acc) rh-op)
                     ; Combine into single multi-arity form
                     (conj acc rh-val)
                     ; Chain new operator on
                     [rh-op acc rh-val]))))))
    expr))

(defn refine-parse-tree
  "Translate DATA from a raw token sequence to an AST, according to its OBJ-TYPE"
  [[obj-type & data]]
  (case obj-type
    :number (edn/read-string (first data))
    (:operator-term
     :operator-factor) ({"+" :add
                         "-" :sub
                         "*" :mult
                         "/" :div} (first data))
    :die-expr (if (= (count data) 2)
                {:count (edn/read-string (first data))
                 :sides (edn/read-string (second data))}
                {:count 1
                 :sides (edn/read-string (first data))})
    (:expr :factor) (if (> (count data) 1)
                      (reorder-operator (mapv refine-parse-tree data))
                      (refine-parse-tree (first data)))))

(defn grab-rolls
  "Get all of the dice rolls from TREE"
  [tree]
  (filter map? (flatten tree)))

(defn eval-roll
  "Compute rolling COUNT number of dice with SIDES number of sides"
  [{:keys [count sides]}]
  (->> (repeatedly #(inc (rand-int sides)))
       (take count)
       vec))

(defn eval-tree
  "Evaluate TREE according to the evaluation engine"
  [tree]
  (condp apply [tree]
    integer?
    {:rolls [] :total tree}
    map?
    (let [roll-res (eval-roll tree)]
      {:rolls [roll-res] :total (reduce + roll-res)})
    sequential?
    (let [[op & args] tree]
      (reduce (fn [acc x]
                (-> acc
                    (update :rolls concat (:rolls x))
                    (update :total ({:mult *
                                     :div /
                                     :add +
                                     :sub -} op)
                            (:total x))))
              (map eval-tree args)))))

(defn eval-roll-tree
  "Evaluate a raw token stream ROLL-TREE into a final result"
  [roll-tree]
  (let [used-roll-tree (if (= (first (last roll-tree)) :comment)
                         (butlast roll-tree)
                         roll-tree)
        refined-roll-tree (refine-parse-tree used-roll-tree)]
    (-> (eval-tree refined-roll-tree)
        (assoc :raw-rolls (grab-rolls (vector refined-roll-tree)))
        (assoc :comment (if (not= used-roll-tree roll-tree)
                          ;; We modified the tree, which means we stripped out a comment
                          (second (last roll-tree)))))))

(defn format-individual-roll
  "Combine RAW and ROLLED results into a pretty-print-ready string"
  [raw rolled]
  (str "• " (:count raw) "d" (:sides raw) " → "
              (if (= (count rolled) 1)
                (str "**" (first rolled))
                (str (clojure.string/join " + " rolled) " = **" (reduce + rolled)))
              "**"))

(defn format-successful-results
  "Write successful roll results into a pretty-print-ready string"
  [{:keys [rolls raw-rolls total comment]}]
  (let [formatted-rolls (mapv format-individual-roll
                              raw-rolls rolls)]
    (str (if (string? comment)
           (str "*[ " (string/trim comment) " ]*\n"))
         (if (pos? (count formatted-rolls))
           (str (string/join "\n" formatted-rolls) "\n"))
         "Final total = ***" total "***\n")))

(defn eval-roll-expr
  "Parse, ASTify, and evaluate ROLL-EXPR"
  [roll-expr]
  (let [roll-tree (expr-parser roll-expr)]
    (condp apply [roll-tree]
      ;; Error condition
      map? (str "Parse error at line "
                (:line roll-tree)
                ", character "
                (inc (:column roll-tree)))
      sequential? (format-successful-results (eval-roll-tree roll-tree)))))
